#!/usr/bin/env bash

yad --title="FastVideo" \
    --image=/opt/fastvideo/tuxcam.svg \
    --text="Press record direction to begin recording based on your current orientation" \
    --form \
    --field="Record rear":fbtn "bash -c '/opt/fastvideo/record.sh rear'" \
    --field="Record front":fbtn "bash -c '/opt/fastvideo/record.sh front'" \
    --field="Open Videos folder":fbtn "bash -c 'xdg-open ${HOME}/Videos/'" \
    --field="Open latest video file":fbtn "bash -c 'cd /opt/fastvideo/ && ./latest.sh'" \
    --field="Delete latest video file":fbtn "bash -c 'cd /opt/fastvideo/ && ./deletelatest.sh'" \
    --field="Rename latest video file":fbtn "bash -c 'cd /opt/fastvideo/ && ./renamelatest.sh'" \
    --buttons-layout=center \
    --button="Quit"\
