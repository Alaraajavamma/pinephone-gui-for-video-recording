#!/usr/bin/env bash

# Specify the folder path where the .mp4 files are located
folder="${HOME}/Videos/"

# Find the latest .mp4 file in the folder
latest_file=$(ls -t "$folder"/*.mp4 2>/dev/null | head -1)

# Check if any .mp4 files were found
if [ -z "$latest_file" ]; then
  echo "No .mp4 files found in the folder."
  exit 1
fi

# Open the latest .mp4 file with mpv
xdg-open "$latest_file"
