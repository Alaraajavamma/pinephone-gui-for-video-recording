# Pinephone GUI for video recording

This is simple tool which allows you to record videos with Pinephone Beta (https://pine64.com/product/pinephone-beta-edition-with-convergence-package/) using GUI buttons. This tool does not provide video preview but it makes video recording so easy that even child can do it 

## What?

Just open app and choose what you want to do. "Everything" should work - if not file an issue or mr 


![Screenshot - default](screenshot1.png)


This should work with all major PP distros 

Downsides: video preview is not possible and video quality is only decent - audio quality is bad. 

Upsides: You can now record videos easily with PP 

Biggest work for this is done by:

@luigi311 - thank you so much :) <hr>
Martijn Braam   (https://blog.brixit.nl/camera-on-the-pinephone/) <hr>
Kevin Kofler (https://forum.manjaro.org/t/pinephone-video-recording-scripts/112993) <hr>
Flax (https://www.youtube.com/watch?v=XvI6Qp5qXN8) <hr>
Peter (https://linmob.net/playing-with-pinephone-video-recording/) <hr>
And many more voluntary people 

## How to install?

`cd ~ && mkdir -p .git && cd .git && git clone https://gitlab.com/Alaraajavamma/fastvideo && cd fastvideo && sudo chmod +x install.sh && ./install.sh `

And it should just work

Remove?

`cd ~ && cd .git && rm -rf fastvideo && rm ~/.local/share/applications/videorec.desktop && sudo rm -rf /opt/fastvideo`


## License
Feel free to do what ever you want with this but no guarantees - this will probably explode your phone xD

## Something else?
If you wan't to help or find issue feel free to contact
