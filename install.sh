#!/usr/bin/env bash

if [ "$(id -u)" -eq 0 ]; then
    echo "Script must not be ran as root"
    exit 1
fi

if command -v "pacman" >/dev/null; then
    sudo pacman -Sy ffmpeg wlr-randr yad v4l-utils
elif command -v "apk" >/dev/null; then
    sudo apk update
    sudo apk add ffmpeg wlr-randr yad v4l-utils
elif command -v "apt" >/dev/null; then
    sudo apt update
    sudo apt install -y ffmpeg wlr-randr yad v4l-utils
else
    echo "Unknown package manager, ffmpeg wlr-randr yad v4l-utils needs to be install via your preferred method"
fi

sudo chmod +x ./*.sh
sudo mkdir -p /opt/fastvideo
sudo ln -s "${PWD}/video.sh" "/opt/fastvideo/"
sudo ln -s "${PWD}/record.sh" "/opt/fastvideo/"
sudo ln -s "${PWD}/capture.sh" "/opt/fastvideo/"
sudo ln -s "${PWD}/deletelatest.sh" "/opt/fastvideo/"
sudo ln -s "${PWD}/delete.svg" "/opt/fastvideo/"
sudo ln -s "${PWD}/renamelatest.sh" "/opt/fastvideo/"
sudo ln -s "${PWD}/rename.svg" "/opt/fastvideo/"
sudo ln -s "${PWD}/latest.sh" "/opt/fastvideo/"
sudo ln -s "${PWD}/tuxcam.svg" "/opt/fastvideo/"

mkdir -p "${HOME}/.local/share/applications/"
ln -s "${PWD}/videorec.desktop" "${HOME}/.local/share/applications/"
